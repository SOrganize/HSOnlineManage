package hso;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import hso.model.Item;
import hso.service.ItemService;

public class ItemTest extends BaseJunit4Test{

	@Resource
	private ItemService itemService;
	
	@Test   //标明是测试方法  
	@Transactional   //标明此方法需使用事务  
	@Rollback(false)  //标明使用完此方法后事务不回滚,true时为回滚  
	public void testItemSave() {
		Item item = new Item();
		item.setContent("111");
		itemService.saveItem(item);
	}
	
	@Test   //标明是测试方法  
	@Transactional   //标明此方法需使用事务  
	@Rollback(false)  //标明使用完此方法后事务不回滚,true时为回滚  
	public void testGetItemList() {
		
		int a = itemService.getItemListByCourseId("18").size();
		System.out.println(a);
	}
	
	
	@Test   //标明是测试方法  
	@Transactional   //标明此方法需使用事务  
	@Rollback(false)  //标明使用完此方法后事务不回滚,true时为回滚  
	public void testItemSpider() {
		
		itemService.spider("http://lt.gdou.com/forumManage.whaty?action=m&bid=311&page=1", 1);
		
	}
	
	
}
