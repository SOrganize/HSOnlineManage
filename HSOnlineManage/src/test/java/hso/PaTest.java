package hso;

import java.util.List;

import org.junit.Test;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class PaTest implements PageProcessor {
	boolean a = false;
	// 部分一：抓取网站的相关配置，包括编码、抓取间隔、重试次数等
	private Site site = Site.me()
			.setRetryTimes(3)
			.setSleepTime(100)
			 //添加cookie之前一定要先设置主机地址，否则cookie信息不生效  
            .setDomain("lt.gdou.com")
            //添加抓包获取的cookie信息  
            .addCookie("FN", "0")
            .addCookie("PN", "0")
            .addCookie("TZ", "R01UKzA4OjAw")
            .addCookie("VM", "0")
            .addCookie("ET", "-1")
            .addCookie("JSESSIONID", "bacNGOMyIjjchcer8Bq_v")
          //添加请求头，有些网站会根据请求头判断该请求是由浏览器发起还是由爬虫发起的  
            .addHeader("User-Agent",  
                    "ozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.80 Safari/537.36 Core/1.47.516.400 QQBrowser/9.4.8188.400")  
            .addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8")  
            .addHeader("Accept-Encoding", "gzip, deflate, sdch").addHeader("Accept-Language", "zh-CN,zh;q=0.8")  
            .addHeader("Connection", "keep-alive").addHeader("Referer", "http://gdou.com/sso/forum/forum_forumManage.action");

	

        @Test
	    public void test() {
	    	 Spider.create(new PaTest())
             //从"https://github.com/code4craft"开始抓
             .addUrl("http://lt.gdou.com/forumManage.whaty?action=m&bid=2903")
             //开启5个线程抓取
             .thread(1)
             //启动爬虫
             .run();
	    	 System.out.println("爬取结束");
	    }

		@Override
		public Site getSite() {
			 return site;
		}


		int count = 0;
		@Override
		public void process(Page page) {
			List<String> list = page.getHtml().css("span.font1 a","text").all();
			int i = 0;
			for(String ss : list){
				i++;
				System.out.println(i+":"+ss);;
			}
		
//	        if (page.getResultItems().get("name") == null) {
//	            //skip this page
//	            page.setSkip(true);
//	        }
	        System.out.println(page.getUrl().toString());
	        count++;
	        // 部分三：从页面发现后续的url地址来抓取
	        if(page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a/@href").all().size()>10) {
	        	if(count<=10)
	        		page.addTargetRequests(page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a[11]/@href").all());
	        	else
	        		page.addTargetRequests(page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a[13]/@href").all());
	        	
	        	System.out.println("-------------------");
	        }else {
	        	List<String> mlist = page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a/@href").all();
	        	mlist.remove(0);
	        	page.addTargetRequests(mlist);
	        }
	        	
		}
	}