package hso;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;

import hso.model.Course;
import hso.service.CourseService;

public class CourseTest extends BaseJunit4Test{

	@Resource
	private CourseService courseService;
	
	@Test   //标明是测试方法  
	@Transactional   //标明此方法需使用事务  
	@Rollback(false)  //标明使用完此方法后事务不回滚,true时为回滚  
	public void test() {
		Course course = new Course();
		course.setCourseid("1");
		course.setLasttime("2017-10-12 12:15:00");
//		course.setName("ce");
//		course.setUrl("---");
//		course.setUsername("jiaoshi");
		//courseService.saveCourse(course);
		//courseService.updateCourse(course);
		int n = courseService.getCourseList().size();
		System.out.println(n);
	}

	
}
