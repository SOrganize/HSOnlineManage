<%@page import="javax.persistence.criteria.CriteriaBuilder.In"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="http://lt.gdou.com/js/jsMsg.jsp"></script>
<script type="text/javascript" src="http://lt.gdou.com/js/xmlHttpRequest.js"></script>
<script type="text/javascript" src="js/ajax.js"></script>
<script language="JavaScript" type="text/javascript">
function checkAll() {
	  var ca = document.getElementById("checkall");
	  var ids = document.getElementsByName("ids");
	  for (var i = 0; i < ids.length; i++) {
	      ids[i].checked = ca.checked;
	  }
	}

function dels() {
	  var ids = document.getElementsByName("ids");
	  var num = 0;
	  var arr = new Array();
	  for (var i = 0; i < ids.length; i++) {
	    if (ids[i].checked) {
	      arr[num] = ids[i].value;
		  num++;
	    }
	  }
	  if (num == 0) {
	    alert("请选择要删除的帖子！");
	    return false;
	  }
	  var del = confirm(confirm_del);
	  if (del) {
		forumManageForm.action.value = "dels";
		forumManageForm.submit();
	    var url='deleteItem';
		var data = 'data=' + arr;
		doAjax(data, url);
	  }
	  else {
	    return false;
	  }
	}
</script>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>删帖</title>
</head>
<body>
<form id="forumManageForm" name="forumManageForm" onsubmit="return true;" action="http://lt.gdou.com/forumManage.whaty" method="post">
<input type="hidden" name="action" value="m" id="forumManageForm_action"/>
<input type="hidden" name="bid" value="${bid}" id="forumManageForm_bid"/>
<input type="hidden" name="page" value="1" id="forumManageForm_page"/>
<c:forEach items="${list}" var="item">
<center><input name="ids" type="checkbox" value="${item.value}" style="zoom:180%;">
<a target="_blank" href="http://lt.gdou.com/read.whaty?action=topic&id=${item.value}&bid=${bid}&fcpage=1&fcaction=index&tagId=0">${item.content}</a>&nbsp;&nbsp;&nbsp;&nbsp;${item.pubname }&nbsp;&nbsp;&nbsp;&nbsp;<span style="font-size:20px">${item.count }</span>
</center>
<br />
</c:forEach>
<div align="center">
        <input id="checkall" type="checkbox" name="checkall" value="checkall" onclick="checkAll();"/>
        选中所有显示的主题
        <input type="button" name="DelsButton" value="删除" class="button1" onclick="dels();" />
      </div>

</form>
<%	int num = (int)request.getAttribute("pageNum");
	int length = (int)request.getAttribute("length");
	//int num = Integer.parseInt(pageNum);
	//int length = Integer.parseInt(pageLength);
	for(int i=0;i<=num;i++){%>
<a href="getItem?bid=${bid}&start=<%=i*length + 1 %>&length=50"><%=i+1 %></a>&nbsp;&nbsp;&nbsp;&nbsp;
<%} %>
</body>
</html>