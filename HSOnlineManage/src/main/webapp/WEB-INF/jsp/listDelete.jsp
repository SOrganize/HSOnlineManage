﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="javax.persistence.criteria.CriteriaBuilder.In"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<TITLE>帖子列表</TITLE> 
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<LINK href="${pageContext.request.contextPath }/css/Style.css" type=text/css rel=stylesheet>
<LINK href="${pageContext.request.contextPath }/css/Manage.css" type=text/css
	rel=stylesheet>
<script type="text/javascript" src="http://lt.gdou.com/js/jsMsg.jsp"></script>
<script type="text/javascript" src="http://lt.gdou.com/js/xmlHttpRequest.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/ajax.js"></script>
<SCRIPT language=javascript>
	function onChange(pageNum)
	{   
		//alert(pageNum);
		//1 将页码的值放入对应表单隐藏域中
		$("#currentPageInput").val(pageNum);
	//2 提交表单
		$("#pageForm").submit();
	}
	function changePageSize(pageSize){
		//1 将页码的值放入对应表单隐藏域中
		//alert(pageSize);
		//alert("当前页"+$("#currentPageInput").val());
		$("#pageSizeInput").val(pageSize);
		
	//2 提交表单
		$("#pageForm").submit();
    }
	//选中所有的文本框
	function checkAll() {
		  var ca = document.getElementById("checkall");
		  var ids = document.getElementsByName("ids");
		  for (var i = 0; i < ids.length; i++) {
		      ids[i].checked = ca.checked;
		  }
		}
    
	//删除
	function dels() {
		  var ids = document.getElementsByName("ids");
		  var num = 0;
		  var arr = new Array();
		  for (var i = 0; i < ids.length; i++) {
		    if (ids[i].checked) {
		      arr[num] = ids[i].value;
			  num++;
		    }
		  }
		  if (num == 0) {
		    alert("请选择要删除的帖子！");
		    return false;
		  }
		  var del = confirm("确定删除？");
		  if (del){
		  	$("#forumManageForm_action").val("dels");
		  	$("#forumManageForm").submit();
		    var url='${pageContext.request.contextPath}/deleteItem';
			var data = 'data=' + arr;
			doAjax(data, url);
		  }
		  else {
		    return false;
		  }
		}
</SCRIPT>

<META content="MSHTML 6.00.2900.3492" name=GENERATOR>
</HEAD>
<BODY>
	
		
		<TABLE cellSpacing=0 cellPadding=0 width="80%" align=center border=0>
			<TBODY>
				<TR>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_019.jpg"
						border=0></TD>
					<TD width="100%" background="${pageContext.request.contextPath }/images/new_020.jpg"
						height=20></TD>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_021.jpg"
						border=0></TD>
				</TR>
			</TBODY>
		</TABLE>
		<TABLE cellSpacing=0 cellPadding=0 align=center width="80%" border=0>
			<TBODY>
				<TR>
					<TD width=15 background=${pageContext.request.contextPath }/images/new_022.jpg><IMG
						src="${pageContext.request.contextPath }/images/new_022.jpg" border=0></TD>
					<TD Align=top width="100%" bgColor=#ffffff>
						<TABLE cellSpacing=0 cellPadding=5 align=center width="80%" border=0>
							<TR>
								<TD class=manageHead>当前位置 :帖子列表</TD>
							</TR>
							<TR>
								<TD height=2></TD>
							</TR>
						</TABLE>
						<TABLE borderColor=#cccccc cellSpacing=0 cellPadding=0
							width="80%" align=center border=0>
							<TBODY>
								<TR>
									<TD height=25>
									  <FORM id="pageForm" name="customerForm" action="${pageContext.request.contextPath}/getItem" method=post>
										<input type="hidden" name="currentpage" id="currentPageInput" value="${pageBean.currentPage}"/>
										<input type="hidden" name="pagesize" id="pageSizeInput" value="${pageBean.pageSize}"/> 
										<input type="hidden" name="bid" id="Bid" value="${bid}"/> 
										<TABLE cellSpacing=0 cellPadding=2 border=0>
											<TBODY>
												<TR>
													<TD>帖子名称：</TD>
													<TD><INPUT class=textbox 
														style="WIDTH: 80px" maxLength=50 name="content" id="content" value="${pageBean.content}"></TD>
													<TD>帖子长度：</TD>
													<TD><INPUT class=textbox 
														style="WIDTH: 80px" maxLength=50 name="length" id="length" value="${pageBean.length}"></TD>
													<TD><INPUT class=button id="sButton2" type="button" 
														value="筛选 " name="sButton2" style="cursor:pointer"  onclick="onChange()"/></TD>
												</TR>
											</TBODY>
										</TABLE>
										</FORM>
									</TD>
								</TR>
							    
								<TR>
									<TD>
									   <form id="forumManageForm" name="forumManageForm" onsubmit="return true;" action="http://lt.gdou.com/forumManage.whaty" method="post">
										<input type="hidden" name="action" value="m" id="forumManageForm_action"/>
                                        <input type="hidden" name="bid" value="${bid}" id="forumManageForm_bid"/>
                                        <input type="hidden" name="page" value="1" id="forumManageForm_page"/>
										<TABLE id=grid
											style="BORDER-TOP-WIDTH: 0px; FONT-WEIGHT: normal; BORDER-LEFT-WIDTH: 0px; BORDER-LEFT-COLOR: #cccccc; BORDER-BOTTOM-WIDTH: 0px; BORDER-BOTTOM-COLOR: #cccccc; WIDTH: 100%; BORDER-TOP-COLOR: #cccccc; FONT-STYLE: normal; BACKGROUND-COLOR: #cccccc; BORDER-RIGHT-WIDTH: 0px; TEXT-DECORATION: none; BORDER-RIGHT-COLOR: #cccccc"
											cellSpacing=1 cellPadding=2 rules=all border=0>
											<TBODY>
												<TR
													style="FONT-WEIGHT: bold; FONT-STYLE: normal; BACKGROUND-COLOR: #eeeeee; TEXT-DECORATION: none;cursor:pointer">
													<TD>选择</TD>
													<TD>帖子长度</TD>
													<TD>帖子名称</TD>
													<TD>发帖人</TD>
													<TD>发帖时间</TD>
												   
													
												</TR>
												<c:forEach items="${pageBean.list}" var="item">
												<TR
													style="FONT-WEIGHT: normal; FONT-STYLE: normal; BACKGROUND-COLOR: white; TEXT-DECORATION: none">
													<TD><input name="ids" type="checkbox" value="${item.value}" style="zoom:180%;"></TD>
													<TD>${item.count }</TD>
													<TD><a target="_blank" href="http://lt.gdou.com/read.whaty?action=topic&id=${item.value}&bid=${bid}&fcpage=1&fcaction=index&tagId=0">${item.content}</a></TD>
													<TD>${item.pubname }</TD>
													<TD>${item.date }</TD>
												</TR>
												
												</c:forEach>
                                  
											</TBODY>
										</TABLE>
								
										</form>
									</TD>
								</TR>
								
								<TR>
									<TD><SPAN id=pagelink>
											<DIV style="LINE-HEIGHT: 20px; HEIGHT: 20px; TEXT-ALIGN: right">
												共[<B>${pageBean.totalRecord}</B>]条记录,共[<B>${pageBean.totalPage}</B>]页
												,每页显示
												<select name="pageSize" onchange="changePageSize($('#pageSizeSelect option:selected').val())" id="pageSizeSelect">
												<option value="30" <c:if test="${pageBean.pageSize eq '30'}">selected</c:if>>30</option>
												<option value="50" <c:if test="${pageBean.pageSize eq '50'}">selected</c:if>>50</option>
												<option value="80" <c:if test="${pageBean.pageSize eq '80'}">selected</c:if>>80</option>
												</select>
												条
												[<A href="javascript:void(0)" onclick="onChange('${pageBean.currentPage-1}')" >前一页</A>]
												<B>${pageBean.currentPage}</B>
												[<A href="javascript:void(0)" onclick="onChange('${pageBean.currentPage+1}')">后一页</A>] 
												到
												<input type="text" size="3" id="page" name="page" />
												页
												<input type="button" value="Go" onclick="onChange($('#page').val())"/>
											</DIV>
									</SPAN></TD>
								</TR>
								<tr style="FONT-WEIGHT: bold; FONT-STYLE: normal; BACKGROUND-COLOR: #eeeeee; TEXT-DECORATION: none" >
								  <td><div align="center">
        <input id="checkall" type="checkbox" name="checkall" value="checkall"  onclick="checkAll();"/>
               选中所有显示的主题
        <input type="button" name="DelsButton" value="删除" class="button1" style="cursor:pointer" onclick="dels();" />
      </div></td>
								</tr>
							</TBODY>
						</TABLE>
						
					</TD>
					<TD width=15 background="${pageContext.request.contextPath }/images/new_023.jpg"><IMG
						src="${pageContext.request.contextPath }/images/new_023.jpg" border=0></TD>
				</TR>
			</TBODY>
		</TABLE>
		<TABLE cellSpacing=0 cellPadding=0 width="80%" align=center border=0>
			<TBODY>
				<TR>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_024.jpg"
						border=0></TD>
					<TD align=middle width="100%"
						background="${pageContext.request.contextPath }/images/new_025.jpg" height=15></TD>
					<TD width=15><IMG src="${pageContext.request.contextPath }/images/new_026.jpg"
						border=0></TD>
				</TR>
			</TBODY>
		</TABLE>
	
</BODY>
</HTML>
