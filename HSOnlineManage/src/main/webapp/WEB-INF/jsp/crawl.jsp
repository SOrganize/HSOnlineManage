<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>爬取课程列表</title>
<link rel="stylesheet" href="css/css.css" type="text/css" />
<script language="javascript" src="js/right.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-1.4.4.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath }/js/ajax.js"></script>

<script type="text/javascript">

  function Delete(id)
  {
	  //alert(id);
	  confirm("确定删除么？");
	  $.ajax({  
          type:"POST",  
          url:"courseDelete",  
          data: {fileId:id},  
          success:function () {  
              window.location.reload();  
          }  
      }) 
  }
</script>
</head>
<body>
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="30"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td height="24" bgcolor="#353c44"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><div align="left">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="14" height="19" valign="bottom"><img src="images/tb.gif" width="14" height="14" /></td>
                    <td width="792" valign="bottom"><div align="left"><span class="STYLE1"> 爬取课程</span></div></td>
                    <td><div align="right"><span class="STYLE1">
			<img src="images/add.gif" width="10" height="10" border="0" /> <a href="${pageContext.request.contextPath}/goaddCourse1">添加</a>  </span>  &nbsp;</div></td>
                  </tr>
              </table>
            </div></td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#a8c7ce" onmouseover="changeto()"  onmouseout="changeback()">
      <tr>
        <td width="10%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">课程id</span></div></td>
       <td width="10%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">课程链接</span></div></td> 
        <!--<td width="10%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">收集数据</span></div></td>-->
        <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">课程名</span></div></td>
   <!--      <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">帖子数目</span></div></td> -->
        <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">最后更新时间</span></div></td>
        <!-- <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">教师最后登录时间</span></div></td>-->
        <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">是否操作</span></div></td> 
         <td width="15%" height="20" bgcolor="d3eaef" class="STYLE6"><div align="center"><span class="STYLE10">操作</span></div></td> 
      </tr>
      
      <c:forEach items="${list }" var="c">
      <tr>
         <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center">${c.courseid}</div></td> 
         <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><a href="http://lt.gdou.com/forum.whaty?action=index&bid=${c.courseid }" target="_blank">${c.name}官网论坛</a></div></td> 
         <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><a href="getItem?bid=${c.courseid }" target="_blank">${c.name }</a></div></td> 
         <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center">${c.lasttime}</div></td> 
         <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><a href="saveItem?courseId=${c.courseid }&lasttime=${c.lasttime}" target="_blank">爬取</a></div></td>
         <td height="20" bgcolor="#FFFFFF" class="STYLE19"><div align="center"><input type="button" value="删除" onclick=Delete(${c.id}) style="cursor:pointer"  /></div></td> 
      </tr>
</c:forEach>
    </table></td>
  </tr>
</table>
</body>
</html>