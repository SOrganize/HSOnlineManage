/**
 * 修改密码
 */
function clickUpdatePwd(){
	parent.frames["mainFrame"].document.getElementById("frmRight").src="http://localhost:8080/scnuNET/updatePassword.html";
}
/**
 * 退出系统
 */
function clickQuit(){
	var returnVal = window.confirm("确认要退出系统吗？","确认");
//	parent.frames["mainFrame"].document.getElementById("frmRight").src="";//topservlet
}
/**
 * 查看个人信息
 */
function clickInfo(){
	parent.frames["mainFrame"].document.getElementById("frmRight").src="";//个人信息jsp
}
/**
 * 返回课程列表页面
 */
function backCourseList(){
	window.location.href="http://localhost:8080/scnuNET/courseList.html";
}

///**
// * 返回新闻列表
// */
//function backNewsList(type){
//	if(type==0){
//		window.location.href="releaseNewsListServlet";
//	}else if(type==1){
//		window.location.href="newsListServlet";
//	}
//}
///**
// * 返回待审核新闻列表
// */
//function backReviewNewsList(){
//	window.location.href="reviewNewsListServlet";
//}
///**
// * 返回菜单列表
// */
//function backMenuList(){
//	window.location.href="menuListServlet";
//}

/**
 * 验证添加课程表单
 */
function checkSaveCourse(){
	if(document.getElementById("name").value==''){
		alert("请输入课程名！");
		return false;
	}
	if(document.getElementById("courseid").value==''){
		alert("请输入课程编码！");
		return false;
	}
	if(document.getElementById("url").value==''){
		alert("请输入课程地址！");
		return false;
	}
	if(document.getElementById("teacher").value==''){
		alert("请输入任课教师！");
		return false;
	}
	if(document.getElementById("teacherurl").value==''){
		alert("请输入任课教师地址！");
		return false;
	}
	if(document.getElementById("login_pwd").value==''){
		alert("请输入最后更新时间！");
		return false;
	}
	
	return true;
}

/**
 * 验证修改密码表单
 */
function checkSavePassword(){
	if(document.getElementById("old_pwd").value==''){
		alert("请输入旧密码！");
		return false;
	}
	if(document.getElementById("login_pwd").value==''){
		alert("请输入新密码！");
		return false;
	}
	if(document.getElementById("confirm_pwd").value==''){
		alert("请输入确认密码！");
		return false;
	}
	if(document.getElementById("login_pwd").value!=document.getElementById("confirm_pwd").value){
		alert("两次输入的密码不一致，请重新输入！");
		return false;
	}
	return true;
}

///**
// * 验证添加新闻类别列表
// * @returns {Boolean}
// */
//function checkSaveNewsType(){
//	if(document.getElementById("name").value==''){
//		alert("请输入类别名称！");
//		return false;
//	}
//	return true;
//}
///**
// * 验证审核新闻信息
// * @returns {Boolean}
// */
//function checkSaveReviewNews(){
//	if(document.getElementById("reviewState").value==''){
//		alert("请选择审核结果！");
//		return false;
//	}
//	return true;
//}
//
//function checkSaveNews(){
//	if(document.getElementById("typeId").value==''){
//		alert("请选择新闻类别！");
//		return false;
//	}
//	if(document.getElementById("title").value==''){
//		alert("请输入标题！");
//		return false;
//	}
//	if(document.getElementById("txtContext").value ==''){
//		alert("请输入新闻内容！");
//		return false;
//	}
//	if(document.getElementById("unit").value==''){
//		alert("请输入发布单位！");
//	}
//	
//	return true;
//}
///**
// * 验证菜单信息
// * @returns {Boolean}
// */
//function checkSaveMenu(){
//	if(document.getElementById("name").value == ''){
//		alert("请输入菜单名称");
//		return false;
//	}
//	return true;
//}
//
///**
// * 根据当前新闻类别更新新闻列表
// */
//function newsTypeSelectChange(val){
//	if(val == ''){
//		window.location.href="newsListServlet?pageNum=1";
//	}else{
//		window.location.href="newsListServlet?pageNum=1&typeId=" + val;
//	}
//}
///**
// * 删除新闻类别
// * @param id
// */
//function clickDeleteNewsType(id){
//	var returnVal = window.confirm("确认要删除此条记录吗？", "确认");
//	if(returnVal) {
//	     window.location.href = "newsTypeDeleteServlet?id=" + id;
//	}
//}
/**
 * 删除单个课程
 * @param id
 */
function clickDeleteCourse(id){
	var returnVal = window.confirm("确认要删除此条记录吗？", "确认");
	if(returnVal) {
	    // window.location.href = "courseDeleteServlet?id=" + id;
	}
}