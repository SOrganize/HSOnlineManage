var xmlHttp;

function doAjax(data, url) {
	xmlHttp = getXMLhttpRequest();
	if (xmlHttp) {
		xmlHttp.open("POST", url, true);
		xmlHttp.setRequestHeader("Content-type",
				"application/x-www-form-urlencoded");
		xmlHttp.onreadystatechange = chuli;
		xmlHttp.send(data);
	}
}

function getXMLhttpRequest() {
	var xmlHttpRequest;
	try {
		xmlHttpRequest = new XMLHttpRequest();
	} catch (e) {
		xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}

	return xmlHttpRequest;
}

function chuli() {
	if (xmlHttp.readyState == 4) {
		if (xmlHttp.status == 200) {
			//刷新网页；
			window.location.reload();
		}
	}
}