package hso.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import hso.model.Course;
import hso.model.Item;
import hso.model.Users;
import hso.service.CourseService;
import hso.service.ItemService;
import hso.service.TestService;
import hso.service.UserService;
import hso.utils.pageBean;
import hso.webmagic.Constant;
/**
 * @描述 该类为测试类
 * @author yu
 *
 */
@Controller
public class TestController {
    @Resource
	private UserService us;  //
	
	/**
	 * 测试服务类成员变量
	 */
	@Resource
    private TestService testService;
	
	@Resource
	private ItemService itemService;
    
	private Integer currentPage;
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public void setContent(String content) {
		Content = content;
	}
	private Integer pageSize;
	private String Content;
	@Resource
	private CourseService courseService;
	/**
	 * @描述 响应/index请求
	 * @return index.jsp view
	 */
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String indexGet(HttpServletRequest request){
        List<Users> list = testService.getAllUser();
        System.out.println("size="+list.size());
        request.setAttribute("size", list.size());
        return "index";
    }
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String indexGet2(HttpServletRequest request){
 
    	return "list";
    }
    @RequestMapping(value = "/list2", method = RequestMethod.GET)
    public String indexGet3(HttpServletRequest request){
    	
    	return "list1";
    }

    /**
     * @描述 拦截/请求（默认请求）
     * @return json default
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public String defaultGet(){
        return "default" ;
    }
    @RequestMapping("/courseDelete")
    public String deleteCourse(Integer fileId)
    {   
    	System.out.println("ajax的"+fileId);
    	courseService.deleteCourse(fileId);
    	return null;
    }
    
    
    @RequestMapping("/saveItem")
    public String saveItem(HttpServletRequest req,String courseId,String lasttime) {
    	String JSessionID=null;
    	Cookie[] cookies = req.getCookies();
    	for(Cookie cookie:cookies)
    	{
    		if(cookie.getName().equals("sessionID"))
    		{
    			JSessionID=cookie.getValue();
    		}
    	}
    	itemService.spiderByDate("http://lt.gdou.com/forumManage.whaty?action=m&bid="+courseId+"&page=1", 1 ,lasttime,JSessionID);
    	return "success";
    }
    //修改帖子列表 
    @RequestMapping("/getItem")
    public String getItemList(HttpServletRequest request,String bid,Integer currentpage,Integer pagesize,String content,Integer length) {
    	//采用离线查询
    	System.out.println(currentpage+","+pagesize+"12");
    	pageBean pb=itemService.getPageBean(currentpage, pagesize,bid,content,length);
    	//List<Item> list = itemService.getItemListByCourseIdPage(bid,start,length);
    	request.setAttribute("list", pb.getList());
    	request.setAttribute("pageBean", pb);
    	request.setAttribute("bid", bid);
    	return "listDelete";
    }
    
    
    //获取帖子列表 
    @RequestMapping("/getItem1")
    public String getItemList1(HttpServletRequest request,String bid,int start,int length) {
    	List<Item> list = itemService.getItemListByCourseIdPage(bid,start,length);
    	request.setAttribute("list", list);
    	request.setAttribute("bid", bid);
    	int num = itemService.getItemNum(bid);
    	int pageNum = 0;
    	if(num%length!=0){
    		pageNum = num/length + 1;
    	}else{
    		pageNum = num / length;
    	}
    	request.setAttribute("pageNum", pageNum);
    	request.setAttribute("length", length);
    	return "delete";
    }
    
    @RequestMapping(value = "/deleteItem", method = RequestMethod.POST)
    @ResponseBody
    public String deleteItemByValue(String [] data){
    	System.out.println("---");
    	for(String ss: data){
    		System.out.println(ss);
    		itemService.deleteItemByValue(ss);
    	}
    	
    	return "true";
    }
    @RequestMapping("/goAddCourse")
    public String goAddCourse(){
    	return "addCourse";
    }
    @RequestMapping("/addCookies")
    public String goAddCookies(HttpServletRequest req,HttpServletResponse response,String SessionId){
    	Cookie cookie=new Cookie("sessionID",SessionId);
    	cookie.setMaxAge(60*180);
    	cookie.setPath("/");
    	response.addCookie(cookie);
    	req.setAttribute("Cookies",SessionId);
    	return "configurationFile";
    }
    @RequestMapping("/configurationFile1")
    public String configurationFile(HttpServletRequest req){
    	String Name=null;
    	Cookie[] cookies = req.getCookies();
    	for(Cookie cookie:cookies)
    	{
    		if(cookie.getName().equals("sessionID"))
    		{
    			Name=cookie.getValue();
    		}
    	}
    	req.setAttribute("Cookies",Name);
    	return "configurationFile";
    }
    
    @RequestMapping("/addCourse")
    public String addCourse(Course course,HttpServletRequest req){
    	String Name=null;
    	Cookie[] cookies = req.getCookies();
    	for(Cookie cookie:cookies)
    	{
    		if(cookie.getName().equals("LoginName"))
    		{
    			course.setUsername(cookie.getValue());
    			Name=cookie.getValue();
    		}
    	}
    	courseService.saveCourse(course);
    	List<Course> list = courseService.getCourseList(Name);
    	req.setAttribute("list",list );
    	return "crawl";
    }
    //获取抓贴课程列表
    @RequestMapping("/getCourseList")
    public String getCourseList(HttpServletRequest req){
    	String Name = null;
    	Cookie[] cookies = req.getCookies();
    	for(Cookie cookie:cookies)
    	{
    		if(cookie.getName().equals("LoginName"))
    		{
    			Name=cookie.getValue();
    		}
    	}
    	
    	req.setAttribute("list", courseService.getCourseList(Name));
    	//return "courseList";
    	return "crawl";
    }
    //转到主页面  前期主要用于测试
   @RequestMapping("/main")
    public String goMain(){
    	return "main";
    }
    //添加课程
    @RequestMapping("/goaddCourse1")
    public String addCourse(){
    	return "addCourse1";
    }

    @RequestMapping("/Login")
    public String Login(HttpServletRequest request,HttpServletResponse response,String name,String password)
    {   
    	Users user=us.getByNamePwd(name, password);
    	if(user!=null)
    	{
    	request.setAttribute("user", user);
    	Cookie cookie=new Cookie("LoginName", user.getName());
    	cookie.setMaxAge(60*240);
    	cookie.setPath("/");
    	response.addCookie(cookie);
    	return "main";
    	}
    	else
    	{
    		return "login";
    	}
    }
}
