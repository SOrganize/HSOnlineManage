package hso.webmagic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import hso.model.Item;
import hso.service.ItemService;
import hso.tool.DateTool;
import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.processor.PageProcessor;

public class ItemSpider extends WebMagicConfig  implements PageProcessor{

	private String url;
	private int threadNum;
	private int count = 0;
	private String lasttime;
	
	
	private ItemService itemService;
	
	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}
	
	public ItemSpider(String cookie,String url,int threadNum) {
		super(cookie);
		this.url = url;
		this.threadNum = threadNum;
	}
	
	@Override
	public Site getSite() {
		System.out.println(site);
		return site;
	}

	@Override
	public void process(Page page) {
		//System.out.println(page);
		System.out.println("----------------------------------------------");
		System.out.println(page.getUrl().toString());
		//item 标题
		List<String> list1 = page.getHtml().css("span.font1 a,span.font1 a font strong","text").all();
		
		//item 字数
		List<String> list2 = page.getHtml().css("td.bgColor4 div","text").all();
		
		//item 用户
		List<String> list3 = page.getHtml().css("td.bgColor4 em","text").all();
		
		//item courseId and itemId
		List<String> list4 = page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table/tbody/tr/td/table/tbody/tr/td[3]/span[1]/a/@href").all();
		
		//item 时间
		List<String> list5 = page.getHtml().css("td.bgColor2 div","text").all();
//		int i = 0;
//		for(String ss : list){
//			i++;
//			System.out.println(i+":"+ss);
//			
//		}
		
		for(int i = 0;i < list1.size(); i++) {
			if(!list1.get(i).startsWith("Re:")) {
				//System.out.println("标题："+list1.get(i));
				//System.out.println("字数："+list2.get(i));
				//System.out.println("用户："+list3.get(i));
//				System.out.println("课程："+list4.get(i));
				if(list1.get(i).equals("")){
					list1.remove(i);
					i--;
					continue;
				}
					
				String[] ss = list4.get(i).split("&");
				String[] sss = ss[1].split("=");
				String value = sss[1];
				String[] ssss = ss[2].split("=");
				String courseId = ssss[1];
				//System.out.println("id:"+courseId+";value:"+value);
				SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String pubdate = sd.format(new Date());
				Item item = new Item();
				item.setCount(Integer.parseInt(list2.get(i)));
				item.setCourseid(courseId);
				item.setValue(value);
				item.setCoursename("无");
				item.setContent(list1.get(i));
				item.setIsread("0");
				item.setDate(list5.get(i));
				item.setPubdate(pubdate);
				item.setPubname(list3.get(i));
				itemService.saveItem(item);
				//itemService.getItemListByCourseId("18");
				//System.out.println("存储："+value);
				//如果判断爬虫时间不在上次爬虫时间之后，则停止爬虫，并记录下爬虫截至时间
				if(!this.checkDate(list5.get(i))){
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
					itemService.updateCourseLasttime(courseId,df.format(new Date()));
					return;
				}
				item = null;
			}
		}
		list1 = null;
		list2 = null;
		list3 = null;
		list4 = null;
		list5 = null;
        
		count++;
		
		
        // 部分三：从页面发现后续的url地址来抓取
        if(page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a/@href").all().size()>10) {
        	if(count<=10)
        		page.addTargetRequests(page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a[11]/@href").all());
        	else
        		page.addTargetRequests(page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a[13]/@href").all());
        	
        	System.out.println("-------------------");
        }else {
        	List<String> mlist = page.getHtml().xpath("//*[@id=\"forumManageForm\"]/table[2]/tbody/tr/td/div/span/a/@href").all();
        	mlist.remove(0);
        	page.addTargetRequests(mlist);
        }
	}
	/**
	 * 为true时才可进行爬虫，代表时间在上次爬虫之后
	 * @param date
	 * @return
	 */
	public boolean checkDate(String date){
		String bdate = null;
		if(date.startsWith("今天")){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String str[] = date.split(" ");
			String str2 = df.format(new Date());
			String str3 = str2 + " " + str[1];
			bdate = str3;
		}else if(date.startsWith("昨天")){
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			String str[] = date.split(" ");
			String str2 = df.format(new Date(new Date().getTime()-24*60*60*1000));
			String str3 = str2 + " " + str[1];
			bdate = str3;
		}else{
			DateFormat df = new SimpleDateFormat("yyyy-");
			String str2 = df.format(new Date());
			String str3 = str2 + date;
			bdate = str3;
		}
		System.out.println(bdate+" =" +lasttime);
		int n = DateTool.compare_date(bdate, lasttime);
		if(n>0)
			return true;
		else
			return false;
	}
	
	
	@Override
	public void start() {
		Spider.create(this)
        .addUrl(url)
        .thread(threadNum)
        //启动爬虫
        .run();		
	}

	@Override
	public void startByDate(String lasttime) {
		this.lasttime = lasttime;
		Spider.create(this)
        .addUrl(url)
        .thread(threadNum)
        //启动爬虫
        .run();	
	}

}
