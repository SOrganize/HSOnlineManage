package hso.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import hso.model.Course;
import hso.model.Item;

@Repository
public class CourseDao {

	@Resource
	private SessionFactory sessionFactory;
	public void saveCourse(Course course) {
		Session session = sessionFactory.openSession();
		session.save(course);
		session.flush();
//		session.clear();
		session.close();
	}
	
	@SuppressWarnings("unchecked")
	public List<Course> getCourseList(String userName){
		String hql = "from Course where username=?";
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(hql);
		query.setString(0, userName);
		session.flush();
		session.clear();
		return query.list();
	}
	@SuppressWarnings("unchecked")
	public List<Course> getCourseList(){
		String hql = "from Course where";
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(hql);
		session.flush();
		session.clear();
		return query.list();
	}
	
	public void updateCourse(Course course){
		Session session = sessionFactory.openSession();
		String hql = "UPDATE Course SET lasttime = ? where courseid = ?";
		Query q = session.createQuery(hql) ;     
        q.setString(0, course.getLasttime());    
        q.setString(1, course.getCourseid());    
        q.executeUpdate() ;   
		session.flush();
//		session.clear();
		session.close();
	}

	public void deleteCourse(Integer id) {
		// TODO Auto-generated method stub
	   Session session = sessionFactory.openSession();
	   String sql="delete from Course where id="+id;
	   Query query = session.createQuery(sql);
	   query.executeUpdate();
	   session.flush();
	   session.close();
	}
}
