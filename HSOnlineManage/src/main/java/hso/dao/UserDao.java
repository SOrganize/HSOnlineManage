package hso.dao;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import hso.model.Users;
@Repository
public class UserDao {
    
	@Resource
    private SessionFactory sessionFactory;  //数据库session操作工厂
	
	public Users getByName(String name)
	{   
		
		String sql="from Users where name=?";
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(sql);
		query.setString(0, name);
		Users user = (Users) query.uniqueResult();
		//session.close();
		session.flush();
		session.clear();
		return user;
	}
}
