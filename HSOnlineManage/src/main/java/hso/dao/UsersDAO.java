package hso.dao;

import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import hso.model.Users;
/**
 * @描述 测试类，用户的模型
 * @author yu
 *
 */
@Repository
public class UsersDAO {
	/**
	 * 数据库session操作工厂
	 */
	@Resource
    private SessionFactory sessionFactory;

	/**
	 * @描述 获得用户列表
	 * @return List<Users>
	 */
    public List<Users> getAllUser(){
        String hsql="from Users";
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(hsql);

        return query.list();
    }
}