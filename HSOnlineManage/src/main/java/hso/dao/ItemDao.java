package hso.dao;


import java.util.List;
import javax.annotation.Resource;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import hso.model.Item;

@Repository
public class ItemDao  {

	@Resource
	private SessionFactory sessionFactory;
	
	public void saveItem(Item item) {
		Session session = sessionFactory.openSession();
		session.save(item);
		session.flush();
//		session.clear();
		session.close();
	}
	
	public List<Item> getItemListByCourseId(String courseId) {
		String hql = "from Item where courseid = " + courseId;
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(hql);
		session.flush();
		session.clear();
		return query.list();
	}
	
	public int getItemNum(String courseId){
		String hql = "from Item where courseid = " + courseId;
		Session session = sessionFactory.openSession();
		Query query = session.createQuery(hql);
		session.flush();
		session.clear();
		return query.list().size();
	}
	
	public List<Item> getItemListByCourseIdPage(String courseId,int start,int length) {
		String hql = "from Item where courseid = " + courseId;
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery(hql);
		query.setFirstResult(start);
		query.setMaxResults(length);
		session.flush();
		session.clear();
		return query.list();
	}
	
	public void deleteItemByValue(String value){
		Session session = sessionFactory.openSession();
		String hql = "Delete FROM Item Where value=?";
		Query q = session.createQuery(hql) ;     
        q.setString(0, value);    
        q.executeUpdate() ;   
		session.flush();
//		session.clear();
		session.close();
	}
	
	//获取帖子总条数
	public Integer getTotalRecord(String content,String bid)
	{    
		if(content==null)
		{
			String sql="select count(*) from Item where courseid=?";
			Session session=sessionFactory.getCurrentSession();
			Query q=session.createQuery(sql);
			q.setString(0, bid);
			Number a = (Number) q.uniqueResult();
			session.flush();
			return a.intValue();
		}else
		{   
			String sql="select count(*) from Item where  content like '%"+content+"%' and courseid="+bid;
			Session session=sessionFactory.getCurrentSession();
			Query q=session.createQuery(sql);
			//q.setString(0,content);
			//q.setString(1, bid);
			Number a = (Number) q.uniqueResult();
			session.flush();
			return a.intValue();
		}
		
		
		
	}
	//获取分页数据
public List<Item> getPageList(int start, Integer pageSize, String bid,String content,Integer length)
   {   
	    if(content==null &&length==null)
	    {
	    	String hql = "from Item where courseid =? ";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setString(0, bid);
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
			session.flush();
			session.clear();
			return query.list();
	    	
	    }else if(length==null &&content!=null)
	    {
	    	String hql = "from Item where courseid =? and content like '%"+content+"%' ";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setString(0, bid);
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
			session.flush();
			session.clear();
			return query.list();
	    }else if(content==null &&length!=null)
	    {
	    	String hql = "from Item t where t.courseid =? and t.count < ?";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setString(0, bid);
			query.setInteger(1,length);
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
			session.flush();
			session.clear();
			return query.list();
	    }else if(content!=null &&length!=null)
	    {
	    	String hql = "from Item t where t.courseid =? and t.count < ? and t.content like '%"+content+"%' ";
			Session session = sessionFactory.getCurrentSession();
			Query query = session.createQuery(hql);
			query.setString(0, bid);
			query.setInteger(1, length);
			query.setFirstResult(start);
			query.setMaxResults(pageSize);
			session.flush();
			session.clear();
			return query.list();
	    }else
	    {
	    	
	    	return null;
	    }
		

		
	
	}
	
}
