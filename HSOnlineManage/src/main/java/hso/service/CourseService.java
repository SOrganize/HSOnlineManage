package hso.service;

import java.util.List;

import hso.model.Course;
import hso.model.Item;

public interface CourseService {

	void updateCourse(Course course);
	
	List<Course> getCourseList(String userName);
	List<Course> getCourseList();
	void saveCourse(Course course);

	void deleteCourse(Integer id);

}
