package hso.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import hso.dao.CourseDao;
import hso.model.Course;

@Service
public class CourseServiceImpl implements CourseService {
	
	@Resource
	private CourseDao courseDao;

	@Override
	public void updateCourse(Course course) {
		courseDao.updateCourse(course);
	}

	@Override
	public List<Course> getCourseList(String userName) {
		return courseDao.getCourseList(userName);
	}
   
	@Override
	public List<Course> getCourseList() {
		return courseDao.getCourseList();
	}
	@Override
	public void saveCourse(Course course) {
		
		
		courseDao.saveCourse(course);
	}

	@Override
	public void deleteCourse(Integer id) {
		// TODO Auto-generated method stub
		courseDao.deleteCourse(id);
	}

	
}
