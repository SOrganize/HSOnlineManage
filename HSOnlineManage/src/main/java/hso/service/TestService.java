package hso.service;

import java.util.List;

import hso.model.Users;
/**
 * @描述 test
 * @author yu
 * 
 */
public interface TestService {
	/**
	 * @描述 获得用户列表
	 * @return List<Users>
	 */
	List<Users> getAllUser();
}
