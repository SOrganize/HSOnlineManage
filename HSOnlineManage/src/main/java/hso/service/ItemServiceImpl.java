package hso.service;


import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;


import hso.dao.CourseDao;
import hso.dao.ItemDao;
import hso.model.Course;
import hso.model.Item;
import hso.utils.pageBean;
import hso.webmagic.Constant;
import hso.webmagic.ItemSpider;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Resource
	private ItemDao itemDao;

	@Resource
	private CourseDao courseDao;

	@Override
	public void saveItem(Item item) {
		itemDao.saveItem(item);
	}

	@Override
	public List<Item> getItemListByCourseId(String courseId) {
		return itemDao.getItemListByCourseId(courseId);
	}

	@Override
	public void spider(String url, int threadNum) {
		ItemSpider itemSpider = new ItemSpider(Constant.COOKIE,url,threadNum);
		itemSpider.setItemService(this);
		itemSpider.start();
//		itemSpider.test();
	}

	@Override
	public void deleteItemByValue(String value) {
		itemDao.deleteItemByValue(value);		
	}
	@Override
	public int getItemNum(String courseId) {
		return itemDao.getItemNum(courseId);
	}
	@Override
	public List<Item> getItemListByCourseIdPage(String courseId, int start, int length) {
		return itemDao.getItemListByCourseIdPage(courseId, start, length);
	}
	@Override
	public void spiderByDate(String url, int threadNum, String lasttime,String JSessionID) {
		ItemSpider itemSpider = new ItemSpider(JSessionID,url,threadNum);
		itemSpider.setItemService(this);
		itemSpider.startByDate(lasttime);
	}

	@Override
	public void updateCourseLasttime(String courseId,String format) {
		Course course = new Course();
		course.setCourseid(courseId);
		course.setLasttime(format);
		courseDao.updateCourse(course);
	}
	
	@Override
	public pageBean getPageBean( Integer currentPage, Integer pageSize,String bid,String content,Integer length) {
		// TODO Auto-generated method stub
		
		//调用Dao查询总记录数
		Integer totalCount=itemDao.getTotalRecord(content,bid);
		//System.out.println(totalCount);
		//创建PageBean对象
		pageBean pb= new pageBean(currentPage,totalCount,pageSize,bid,content,length);
		System.out.println(currentPage+","+totalCount+","+pageSize+","+bid+","+content+"PageBean对象"+length);
		//调用Dao查询分页列数据
		List<Item> list=itemDao.getPageList(pb.getStart(),pb.getPageSize(),bid,content,length);
		System.out.println(pb.getStart()+","+pb.getPageSize()+","+bid);
		pb.setList(list);
		return pb;
	}


}
