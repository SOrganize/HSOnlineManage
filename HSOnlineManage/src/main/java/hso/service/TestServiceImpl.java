package hso.service;

import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import hso.dao.UsersDAO;
import hso.model.Users;
/**
 * @描述 test实现类
 * @author iceyu
 *
 */
@Service
public class TestServiceImpl implements TestService {

	/**
	 * 用户Dao
	 */
	@Resource
	private UsersDAO usersDao;
	
	@Override
	public List<Users> getAllUser() {
		return usersDao.getAllUser();
	}

}
