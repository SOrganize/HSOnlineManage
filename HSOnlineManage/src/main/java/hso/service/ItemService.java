package hso.service;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import hso.model.Item;
import hso.utils.pageBean;

public interface ItemService {

	 void saveItem(Item item);
	 
	 List<Item> getItemListByCourseId(String courseId);
	 
	 void spider(String url,int threadNum);
	 
	 void spiderByDate(String url,int threadNum,String lasttime,String JSessionID);
	 
	 void deleteItemByValue(String value);
	 
	 int getItemNum(String courseId);
	 
	 List<Item> getItemListByCourseIdPage(String courseId,int start,int length);

	void updateCourseLasttime(String courseId,String format);
	pageBean getPageBean( Integer currentPage, Integer pageSize,String bid,String Content,Integer length);
}
