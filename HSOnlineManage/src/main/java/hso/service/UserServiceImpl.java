package hso.service;

import javax.annotation.Resource;
import javax.management.RuntimeErrorException;

import org.springframework.stereotype.Service;

import hso.dao.UserDao;
import hso.model.Users;
@Service
public class UserServiceImpl implements UserService {
     @Resource
	 private UserDao ud;
	@Override
	public Users getByNamePwd(String username, String password) {
		Users user=ud.getByName(username);
		return user;
	}
   
}
