package hso.utils;

import java.util.List;

public class pageBean {
	private Integer totalRecord;// 表示查询后一共得到多少条结果记录
    private Integer pageSize; // 表示页面一次要显示多少条记录
    private Integer totalPage;// 表示将所有的记录进行分页后，一共有多少页
    private Integer startIndex;// 表示从所有的结果记录中的哪一个编号开始分页查询
    private Integer currentPage; // 表示用户想看的页数
    private String bid;
    private String content;
    private Integer length;
    public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getBid() {
		return bid;
	}
	public void setBid(String bid) {
		this.bid = bid;
	}
	private List list;// list集合是用来装载一个页面中的所有记录的

    public pageBean(Integer currentPage, Integer totalRecord,Integer pageSize,String bid,String content,Integer length) {
        this.currentPage = currentPage;
        this.totalRecord = totalRecord;
        this.pageSize=pageSize;
        this.bid=bid;
        this.length=length;
        if(this.currentPage==null)
        {
        	this.currentPage=1;
        }
        if(this.pageSize==null)
        {
        	 this.pageSize = 30;// 设置一页默认显示30条查询记录
        }
        
        
       
        this.startIndex = (this.currentPage - 1) * this.pageSize;// 至于为什么this.page要减1，
        // 是因为mysql数据库对于分页查询时，得到的所有的查询记录，第一条记录的编号是从0开始。
        this.totalPage=((int)this.totalRecord+this.pageSize-1)/this.pageSize;
        if(this.currentPage<1)
    	{
    		this.currentPage=1;
    	}
    	if(this.currentPage>this.totalPage)
    	{
    		this.currentPage=this.totalPage;
    	}
       }
    public int getStart(){   //计算起始索引
    	return (this.currentPage-1)*this.pageSize;
    }
	
	public Integer getTotalRecord() {
		return totalRecord;
	}
	public void setTotalRecord(Integer totalRecord) {
		this.totalRecord = totalRecord;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getTotalPage() {
		return totalPage;
	}
	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}
	public Integer getStartIndex() {
		return startIndex;
	}
	public void setStartIndex(Integer startIndex) {
		this.startIndex = startIndex;
	}
	public Integer getCurrentPage() {
		return currentPage;
	}
	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}
	public List getList() {
		return list;
	}
	public void setList(List list) {
		this.list = list;
	}
	

}
